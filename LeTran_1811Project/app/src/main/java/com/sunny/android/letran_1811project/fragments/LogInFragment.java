package com.sunny.android.letran_1811project.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

import java.io.FileOutputStream;
import java.util.Objects;

public class LogInFragment extends Fragment {

	// Member variable
	private FirebaseAuth mAuth;
	private NotifyActivity mListener;
	private Button btn_LogIn;

	// Initialize FirebaseAuth
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof NotifyActivity) {
			mListener = (NotifyActivity)context;
		}
	}

	// Constructor
	public static LogInFragment newInstance() {
		return new LogInFragment();
	}

	// Inflate view layout of fragment
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_log_in, container, false);
	}

	// Assign action to log in button which would be checking if email and password have the correct info
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if(getView() != null && getContext() != null && getActivity() != null) {
			final EditText email = getView().findViewById(R.id.etx_Email);
			final EditText password = getView().findViewById(R.id.etx_Password);
			btn_LogIn = getView().findViewById(R.id.btn_login);
			btn_LogIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (MethodHelpers.isConnected(getContext())) {
						String emailStr = email.getText().toString();
						String passwordStr = password.getText().toString();

						int clear = MethodHelpers.checkIfTextsEmptyOrSpaced(emailStr, passwordStr, "",
								"", "",  0);
						if (clear == 0) {
							signIn(emailStr, passwordStr);
						} else {
							Toast.makeText(getContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
					}
				}
			});

			EditText[] texts = {email, password};
			for (EditText editText: texts) {
				editText.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						btn_LogIn.setEnabled(true);
					}
				});
			}
		}
	}

	// Function to sign in existing users
	private void signIn(String email, String password) {
		if (mAuth != null && getActivity() != null) {
			mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(),
					new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if (task.isSuccessful()) {
								btn_LogIn.setEnabled(false);
								Toast.makeText(getActivity(), "Successfully logged in!", Toast.LENGTH_SHORT).show();
								if (mListener != null && getContext() != null) {
									try {
										FileOutputStream fos = getContext().openFileOutput("listie.txt", Context.MODE_PRIVATE);
										String signedIn = "Signed in";
										fos.write(signedIn.getBytes());
										fos.close();

										mListener.newScreen(null, null,null);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							} else {
								try {
									throw Objects.requireNonNull(task.getException());
								} catch (FirebaseAuthInvalidUserException noEmail) {
									Toast.makeText(getActivity(), "Your email is not registered",
											Toast.LENGTH_SHORT).show();
								} catch (FirebaseAuthInvalidCredentialsException wrongPass) {
									Toast.makeText(getActivity(), "Your password or your email is incorrect",
											Toast.LENGTH_SHORT).show();
								} catch (Exception e) {
									e.printStackTrace();
									Toast.makeText(getActivity(), "Error occurred. Please try again",
											Toast.LENGTH_SHORT).show();
								}
							}
						}
					});
		}
	}
}
