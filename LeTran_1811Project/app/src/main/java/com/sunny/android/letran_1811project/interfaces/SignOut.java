package com.sunny.android.letran_1811project.interfaces;

// Interface to inform current activity that user wants to sign out and carry out appropriate action
public interface SignOut {
	void signOut();
}
