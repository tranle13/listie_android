package com.sunny.android.letran_1811project.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.custom_objects.User;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;
import com.sunny.android.letran_1811project.interfaces.RemoveOrAccept;

import java.util.ArrayList;
import java.util.Objects;

public class FriendRequestAdapter extends BaseAdapter {

	// Member variables
	private final Context mContext;
	private final ArrayList<User> requests;
	private RemoveOrAccept mListener;
	private final boolean isTask;

	// Constructor
	public FriendRequestAdapter(Context _context, ArrayList<User> _requests, boolean _isTask) {
		mContext = _context;
		requests = _requests;
		isTask = _isTask;

		if (_context instanceof RemoveOrAccept) {
			mListener = (RemoveOrAccept) _context;
		}
	}

	// Function to return the amount of objects
	@Override
	public int getCount() {
		if (requests != null) {
			return requests.size();
		}
		return 0;
	}

	// Function to return item at specific position
	@Override
	public Object getItem(int position) {
		if (requests != null && 0 <= position || position < Objects.requireNonNull(requests).size()) {
			return requests.get(position);
		}
		return null;
	}

	// Function to return item unique ID
	@Override
	public long getItemId(int position) {
		return 0x1101 + position;
	}

	// Function to build recycle view for ListView
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_friendrequest_listrow,
					parent, false);
			vh = new ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		if (requests.get(position) != null) {
			vh.txt_FriendName.setText(requests.get(position).getFullName());
			vh.ivw_FriendRequestImage.setImageBitmap(MethodHelpers.getRoundedCornerBitmap(
					BitmapFactory.decodeByteArray(requests.get(position).getProfilePicture(),
							0, requests.get(position).getProfilePicture().length)
			));

			vh.btn_RemoveRequest.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.removeOrAccept(requests.get(position).getUid(), false);
					}
				}
			});

			if (isTask) {
				vh.btn_AcceptRequest.setVisibility(View.GONE);
			} else {
				vh.btn_AcceptRequest.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mListener != null) {
							mListener.removeOrAccept(requests.get(position).getUid(), true);
						}
					}
				});
			}
		}

		return convertView;
	}

	// Recycle view
	static class ViewHolder {
		final TextView txt_FriendName;
		final ImageButton btn_RemoveRequest;
		final ImageButton btn_AcceptRequest;
		final ImageView ivw_FriendRequestImage;

		ViewHolder(View layout) {
			txt_FriendName = layout.findViewById(R.id.txt_FriendName);
			btn_RemoveRequest = layout.findViewById(R.id.btn_RemoveRequest);
			btn_AcceptRequest = layout.findViewById(R.id.btn_AcceptRequest);
			ivw_FriendRequestImage = layout.findViewById(R.id.ivw_FriendRequestImage);
		}
	}
}
