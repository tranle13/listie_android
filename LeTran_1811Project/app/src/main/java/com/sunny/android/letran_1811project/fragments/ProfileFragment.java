package com.sunny.android.letran_1811project.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;
import com.sunny.android.letran_1811project.interfaces.SignOut;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Objects;

public class ProfileFragment extends Fragment {

	// Member variables
	private FirebaseAuth mAuth;
	private static final String TAG = "ProfileFragment";
	private SignOut mListener;
	private boolean isEditMode = false;
	private static final int REQUEST_IMAGE = 0x1010;
	private ImageView ivw_Image;
	private Bitmap defaultImage;
	private static final int REQUEST_EXTERNAL_PERMISSION = 0x0111;

	// Constructor
	public static ProfileFragment newInstance() {
		return new ProfileFragment();
	}

	// Set options menu and initialize FirebaseAuth
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		mAuth = FirebaseAuth.getInstance();
	}

	// Check if the host context implements interface
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof SignOut) {
			mListener = (SignOut) context;
		}
	}

	// Function to inflate fragment layout
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_profile, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && mAuth.getUid() != null && getContext() != null && getActivity() != null) {
			final Button signOut = getView().findViewById(R.id.btn_SignOut);
			final EditText fullname = getView().findViewById(R.id.etx_FullName);
			final EditText username = getView().findViewById(R.id.etx_Username);
			final EditText email = getView().findViewById(R.id.etx_Email);
			ivw_Image = getView().findViewById(R.id.ivw_ProfileImage);
			defaultImage = BitmapFactory.decodeResource(getResources(), R.drawable.profile_default);

			// Code to get stored image from Firebase and display it
			final StorageReference stoRef = FirebaseStorage.getInstance().getReference().child(mAuth.getUid());
			stoRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
				@Override
				public void onSuccess(byte[] bytes) {
					defaultImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
				}
			}).addOnCompleteListener(new OnCompleteListener<byte[]>() {
				@Override
				public void onComplete(@NonNull Task<byte[]> task) {
					ivw_Image.setImageBitmap(MethodHelpers.getRoundedCornerBitmap(defaultImage));
				}
			});

			ivw_Image.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (ContextCompat.checkSelfPermission(getContext(),
							Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
						startImageActivity();
					} else {
						ActivityCompat.requestPermissions(getActivity(), new String[]
								{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_PERMISSION);
					}
				}
			});

			// Code to get info from Firebase
			FirebaseDatabase.getInstance().getReference().child(mAuth.getUid())
					.addListenerForSingleValueEvent(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					fullname.setText(Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString());
					username.setText(Objects.requireNonNull(dataSnapshot.child("username").getValue()).toString());
					email.setText(Objects.requireNonNull(mAuth.getCurrentUser()).getEmail());
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});

			// Code to sign out or update info when button is clicked
			signOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (signOut.getText().toString().toLowerCase().equals("sign out") && mListener != null) {
						FirebaseAuth.getInstance().signOut();
						if (getContext() != null) {
							getContext().deleteFile("listie.txt");
						}
						mListener.signOut();
					} else {
						DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(mAuth.getUid());
						ref.child("fullname").setValue(fullname.getText().toString());
						ref.child("username").setValue(username.getText().toString());
						signOut.setText(R.string.btn_signout);
						setHasOptionsMenu(true);
						getView().findViewById(R.id.etx_FullName).setEnabled(false);
						getView().findViewById(R.id.etx_Username).setEnabled(false);
						isEditMode = false;
						Toast.makeText(getContext(), "Saved successfully!", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	// Code to ask for permission again if user hasn't allowed
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			startImageActivity();
		}
	}

	// Code to save the image to FirebaseStorage before fragment is destroyed
	@Override
	public void onPause() {
		super.onPause();
		if (mAuth.getUid() != null) {
			FirebaseStorage storage = FirebaseStorage.getInstance();
			StorageReference imageRef = storage.getReference().child(mAuth.getUid());

			Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.profile_default);
			if (ivw_Image.getDrawable() != null) {
				bmp = ((BitmapDrawable) ivw_Image.getDrawable()).getBitmap();
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			imageRef.putBytes(baos.toByteArray());
		}
	}

	// Code to get image URI from gallery after going back to app
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == -1) {
			Uri chosenUri = data.getData();
			if (chosenUri != null && getActivity() != null) {
				try {
					InputStream imageStream = getActivity().getContentResolver().openInputStream(chosenUri);
					Bitmap chosenImage = BitmapFactory.decodeStream(imageStream);
					ivw_Image.setImageBitmap(MethodHelpers.getRoundedCornerBitmap(chosenImage));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	// Inflate menu layout
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_edit, menu);
	}

	// Assign action to options item
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_edit && getView() != null) {
			getView().findViewById(R.id.etx_FullName).setEnabled(true);
			getView().findViewById(R.id.etx_Username).setEnabled(true);
			((Button)getView().findViewById(R.id.btn_SignOut)).setText(R.string.menu_save);
			setHasOptionsMenu(false);
			isEditMode = true;
		}
		return super.onOptionsItemSelected(item);
	}

	// Function to get gallery for user to choose image from
	private void startImageActivity() {
		if (isEditMode) {
			Intent imageIntent = new Intent(Intent.ACTION_PICK);
			imageIntent.setType("image/*");
			startActivityForResult(imageIntent, REQUEST_IMAGE);
		}
	}
}
