package com.sunny.android.letran_1811project.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

import java.io.FileOutputStream;
import java.util.Objects;

public class SignUpFragment extends Fragment {

	// Member variables
	private FirebaseAuth mAuth;
	private FirebaseUser mUser;
	private NotifyActivity mListener;
	private Button btn_SignUp;

	// Constructor
	public static SignUpFragment newInstance() {
		return new SignUpFragment();
	}

	// Initialize FirebaseAuth
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
	}

	// Code to check if host context implements necessary interface
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof NotifyActivity) {
			mListener = (NotifyActivity)context;
		}
	}

	// Inflate view layout of fragment
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_sign_up, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null) {

			// Code to get UI components, their values and carry out respective action
			final EditText email = getView().findViewById(R.id.etx_Email);
			final EditText username = getView().findViewById(R.id.etx_Username);
			final EditText password = getView().findViewById(R.id.etx_Password);
			final EditText fullName = getView().findViewById(R.id.etx_FullName);
			final EditText confirmPass = getView().findViewById(R.id.etx_ConfirmPass);

			btn_SignUp = getView().findViewById(R.id.btn_SignUp);
			btn_SignUp.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (MethodHelpers.isConnected(getContext())) {
						final String emailStr = email.getText().toString();
						final String usernameStr = username.getText().toString();
						final String passwordStr = password.getText().toString();
						final String fullNameStr = fullName.getText().toString();
						final String confirmPassStr = confirmPass.getText().toString();

						int clear = MethodHelpers.checkIfTextsEmptyOrSpaced(emailStr, passwordStr,
								usernameStr, fullNameStr, confirmPassStr, 1);
						if (clear == 0) {
							createNewAccount(emailStr, passwordStr, usernameStr, fullNameStr);
						} else if (clear == 2) {
							Toast.makeText(getContext(), "Please reconfirm your password", Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(getContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
					}
				}
			});

			EditText[] texts = {email, password, username, fullName, confirmPass};
			for (EditText editText: texts) {
				editText.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						btn_SignUp.setEnabled(true);
					}
				});
			}
		}
	}

	// Function to create new account on Firebase with email and password
	private void createNewAccount(String email, String password, final String username, final String fullName) {
		if (mAuth != null && getActivity() != null) {
			mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(),
					new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if (task.isSuccessful()) {
								btn_SignUp.setEnabled(false);
								Toast.makeText(getActivity(), "Successfully signed up!", Toast.LENGTH_SHORT).show();
								if (mListener != null && getContext() != null) {
									try {
										FileOutputStream fos = getContext().openFileOutput("listie.txt", Context.MODE_PRIVATE);
										String signedIn = "Signed in";
										fos.write(signedIn.getBytes());
										fos.close();
									} catch (Exception e) {
										e.printStackTrace();
									}
									mListener.newScreen(null, null,null);
								}

								mUser = mAuth.getCurrentUser();
								FirebaseDatabase db = FirebaseDatabase.getInstance();
								final DatabaseReference dbRef = db.getReference().child(mUser.getUid());
								dbRef.child("username").setValue(username);
								dbRef.child("fullname").setValue(fullName);
									
							} else {
								try {
									throw Objects.requireNonNull(task.getException());
								} catch (FirebaseAuthWeakPasswordException weakPass) {
									Toast.makeText(getActivity(), "Your password is too weak",
											Toast.LENGTH_SHORT).show();
								} catch (FirebaseAuthInvalidCredentialsException malformedEmail) {
									Toast.makeText(getActivity(), "Your email is malformed",
											Toast.LENGTH_SHORT).show();
								} catch (FirebaseAuthUserCollisionException existEmail) {
									Toast.makeText(getActivity(), "This is email was already registered",
											Toast.LENGTH_SHORT).show();
								}
								catch (Exception e) {
									e.printStackTrace();
									Toast.makeText(getActivity(), "Error occurred. Please try again",
											Toast.LENGTH_SHORT).show();
								}
							}
						}
					});
		}
	}
}
