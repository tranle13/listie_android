package com.sunny.android.letran_1811project.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.adapters.ListAdapter;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class ListsFragment extends Fragment {

	// Member variables
	private ConstraintLayout emptyState;
	private RecyclerView recyclerView;
	private static final String TAG = "ListsFragment";
	private static boolean isTask;
	private final int[] colors = {R.color.blue, R.color.bright_orange, R.color.bright_yellow,
			R.color.colorPrimaryDark, R.color.green, R.color.green_turquoise, R.color.light_orange,
			R.color.light_pink, R.color.light_yellow, R.color.lucid_red, R.color.neon_blue,
			R.color.pastel_blue, R.color.pastel_pink, R.color.pink, R.color.pure_yellow, R.color.red,
			R.color.white};

	// Constructor
	public static ListsFragment newInstance(boolean _isTask) {
		isTask = _isTask;
		return new ListsFragment();
	}

	// Set hasOptionsMenu based on cases
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!isTask) {
			setHasOptionsMenu(true);
		}
	}

	// Inflate fragment's layout
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_lists, container, false);
	}

	// Function to get UI components and assign actions to them
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null && getActivity() != null) {
			emptyState = getView().findViewById(R.id.emptyState);
			recyclerView = getView().findViewById(R.id.rvw_RecyclerView);

			final String uuid = FirebaseAuth.getInstance().getUid();
			if (uuid != null) {
				DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
				checkEmptyState(ref, uuid);
			}
		}
	}

	// Inflate menu layout
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_add, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	// Assign respective action to options items
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_add) {
			if (getActivity() != null) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

				LayoutInflater inflater = getActivity().getLayoutInflater();
				View dialogView = inflater.inflate(R.layout.custom_alertdialog, null);
				final EditText newListName = dialogView.findViewById(R.id.etx_NewListName);
				builder.setView(dialogView);
				builder.setTitle("New List");
				builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
						String trimmedName = newListName.getText().toString().replace(" ", "");
						if (mUser != null && trimmedName.length() > 0) {
							DatabaseReference ref = FirebaseDatabase.getInstance()
									.getReference(mUser.getUid()).child("lists")
									.child(newListName.getText().toString());
							Random rnd = new Random();
							int index = rnd.nextInt(colors.length);
							ref.child("color").setValue(index);
						} else {
							Toast.makeText(getContext(), "Invalid list name", Toast.LENGTH_SHORT).show();
						}
					}
				});

				builder.setNegativeButton("Cancel", null);
				builder.create().show();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	// Function to check if the list of lists is empty and pull data down if there is any
	private void checkEmptyState(DatabaseReference ref, final String uuid) {
		if (recyclerView != null) {
			ref.addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					ArrayList<String> lists = new ArrayList<>();
					ArrayList<Integer> indices = new ArrayList<>();
					ArrayList<String> uids = new ArrayList<>();
					DataSnapshot shot;

					if (isTask) {
						shot = dataSnapshot.child(uuid).child("assignedlists");
						for (DataSnapshot snapshot : shot.getChildren()) {
							lists.add(Objects.requireNonNull(snapshot.getValue()).toString());
							uids.add(snapshot.getKey());
						}

						for (int i = 0; i < lists.size(); i++) {
							Integer index = Integer.parseInt(Objects.requireNonNull(dataSnapshot.child(uids.get(i)).child("lists")
									.child(lists.get(i)).child("color").getValue()).toString());
							indices.add(index);
						}
					} else {
						shot = dataSnapshot.child(uuid).child("lists");
						for (DataSnapshot snapshot : shot.getChildren()) {
							String key = snapshot.getKey();

							if (snapshot.child("color").getValue() != null) {
								Integer index = Integer.parseInt(Objects.requireNonNull(snapshot.child("color").getValue()).toString());
								indices.add(index);
							}
							lists.add(key);
						}
					}

					if (lists.size() <= 0) {
						emptyState.setVisibility(View.VISIBLE);
					} else {
						emptyState.setVisibility(View.INVISIBLE);
					}

					ListAdapter adapter = new ListAdapter(lists, getContext(), indices, isTask, uids);
					recyclerView.setAdapter(adapter);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});
		}
	}
}
