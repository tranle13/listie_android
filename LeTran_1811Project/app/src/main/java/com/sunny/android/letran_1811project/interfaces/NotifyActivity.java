package com.sunny.android.letran_1811project.interfaces;

// Interface to communicate with host activity
public interface NotifyActivity {
	void newScreen(String string, String _string, Boolean isTask);

	interface Changes {
		void changeChecked(boolean isChecked, String id, String uid);
		void hideTheFab();
	}
}
