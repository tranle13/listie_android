package com.sunny.android.letran_1811project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.fragments.FriendsFragment;
import com.sunny.android.letran_1811project.fragments.ListsFragment;
import com.sunny.android.letran_1811project.fragments.ProfileFragment;
import com.sunny.android.letran_1811project.interfaces.RemoveOrAccept;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;
import com.sunny.android.letran_1811project.interfaces.SignOut;

public class ListsActivity extends AppCompatActivity implements NotifyActivity, RemoveOrAccept, SignOut {

	// Member variables
	public static final String EXTRA_LIST_NAME = "com.sunny.android.letran_1811project.EXTRA_LIST_NAME";
	public static final String EXTRA_IF_TASK = "com.sunny.android.letran_1811project.EXTRA_IF_TASK";
	public static final String EXTRA_UID = "com.sunny.android.letran_1811project.EXTRA_UID";
	private FirebaseAuth mAuth;

	// Create listener for bottom navigation bar
	private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
			= new BottomNavigationView.OnNavigationItemSelectedListener() {

		@Override
		public boolean onNavigationItemSelected(@NonNull MenuItem item) {
			switch (item.getItemId()) {
				case R.id.tab_btn_lists:
					getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolder,
							ListsFragment.newInstance(false)).commit();
					if (getSupportActionBar() != null) {
						getSupportActionBar().setTitle("Lists");
					}
					return true;
				case R.id.tab_btn_tasks:
					getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolder,
							ListsFragment.newInstance(true)).commit();
					if (getSupportActionBar() != null) {
						getSupportActionBar().setTitle("Tasks");
					}
					return true;
				case R.id.tab_btn_friends:
					getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolder,
							FriendsFragment.newInstance(false, null)).commit();
					if (getSupportActionBar() != null) {
						getSupportActionBar().setTitle("Friends");
					}
					return true;
				case R.id.tab_btn_profile:
					getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolder,
							ProfileFragment.newInstance()).commit();
					if (getSupportActionBar() != null) {
						getSupportActionBar().setTitle("Profile");
					}
					return true;
			}
			return false;
		}
	};

	// Set up UI in onCreate
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lists);

		// Assign action to bottom navigation view and initialize FirebaseAuth
		BottomNavigationView navigation = findViewById(R.id.navigation);
		navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
		mAuth = FirebaseAuth.getInstance();

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle("Lists");
		}
		getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolder,
				ListsFragment.newInstance(false)).commit();
	}

	// Listener method to go to new activity
	@Override
	public void newScreen(String string, String _string, Boolean isTask) {
		Intent newIntent = new Intent(this, ItemListActivity.class);
		newIntent.putExtra(EXTRA_LIST_NAME, string);
		newIntent.putExtra(EXTRA_IF_TASK, isTask);
		newIntent.putExtra(EXTRA_UID, _string);
		startActivity(newIntent);
	}

	// Interface to carry out action if user accepts or remove friend request
	@Override
	public void removeOrAccept(String uid, boolean isAccepted) {
		if (mAuth.getUid() != null) {
			DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

			if (isAccepted) {
				ref.child(mAuth.getUid()).child("friends").child(uid).setValue("");
				ref.child(uid).child("friends").child(mAuth.getUid()).setValue("");
			}
			ref.child(mAuth.getUid()).child("requests").child(uid).removeValue();
		}
	}

	// Dismiss current activity and call ActivityActivity if user signs out
	@Override
	public void signOut() {
		Intent newIntent = new Intent(this, ActivityActivity.class);
		startActivity(newIntent);
		finish();
	}
}
