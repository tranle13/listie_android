package com.sunny.android.letran_1811project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.fragments.ItemListFragment;
import com.sunny.android.letran_1811project.fragments.NewItemFragment;
import com.sunny.android.letran_1811project.interfaces.DeleteItems;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;
import com.sunny.android.letran_1811project.interfaces.RemoveOrAccept;

import java.util.ArrayList;

public class ItemListActivity extends AppCompatActivity implements NotifyActivity,
		NotifyActivity.Changes, DeleteItems, RemoveOrAccept {

	// Member variables
	private Animation scaleUp;
	private Animation scaleDown;
	private String listName;
	private boolean isTask;
	private String uid;
	private FloatingActionButton fab;
	private boolean isFinished = true;
	private ArrayList<String> ids = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_list);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		// Initialize animations for floating action button
		scaleUp = AnimationUtils.loadAnimation(this, R.anim.scale_up);
		scaleDown = AnimationUtils.loadAnimation(this, R.anim.scale_down);

		// Get data from start intent from ListActivity
		Intent startIntent = getIntent();
		listName = startIntent.getStringExtra(ListsActivity.EXTRA_LIST_NAME);
		isTask = startIntent.getBooleanExtra(ListsActivity.EXTRA_IF_TASK, false);
		uid = startIntent.getStringExtra(ListsActivity.EXTRA_UID);

		// Change action bar title according to the present fragment
		if (getSupportActionBar() != null && listName != null) {
			getSupportActionBar().setTitle(listName);
		}

		// Initialize floating action button
		fab = findViewById(R.id.fab);

		// This fragment is used for Lists and Tasks, if it's for Lists, add new fragment when user
		// clicks on floating action button. If it's not then hide fab
		if (!isTask) {
			fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.itemListFragmentContainer,
							NewItemFragment.newInstance(listName))
							.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
							.commit();
					hideFab();
				}
			});
		} else {
			fab.hide();
		}

		getSupportFragmentManager().beginTransaction().replace(R.id.itemListFragmentContainer,
				ItemListFragment.newInstance(listName, isTask, uid)).commit();
	}

	// Dismiss the current activity
	@Override
	public void onBackPressed() {
		if (!isTask) {
			FloatingActionButton fab = findViewById(R.id.fab);
			fab.startAnimation(scaleUp);
			fab.show();
			if (!isFinished) {
				getSupportFragmentManager().popBackStackImmediate();
				getSupportFragmentManager().beginTransaction().replace(R.id.itemListFragmentContainer,
						ItemListFragment.newInstance(listName, isTask, uid)).commit();
				isFinished = true;
			} else {
				super.onBackPressed();
			}
		}  else {
			super.onBackPressed();
		}
	}

	// Function to go dismiss view if Save button in NewItemFragment is pressed when conditions
	// are met
	@Override
	public void newScreen(String string, String _string, Boolean isTask) {
		onBackPressed();
	}

	// Function to update state of item (checked or unchecked)
	@Override
	public void changeChecked(boolean isChecked, String id, String uid) {
		DatabaseReference ref = FirebaseDatabase.getInstance().getReference(uid)
				.child("lists").child(listName).child("items").child(id);
		ref.child("checked").setValue(isChecked);
	}

	// Function to hide floating button
	private void hideFab() {
		if (fab != null) {
			fab.startAnimation(scaleDown);
			fab.hide();
		}
	}

	// Function to hide the floating action button and clear the collection of selected items' IDs
	@Override
	public void hideTheFab() {
		hideFab();
		isFinished = false;
		ids.clear();
	}

	// Function to collect the IDs of the selected items to delete them later
	@Override
	public ArrayList<String> deleteItem(String id, boolean isNow) {
		if (!isNow && id != null) {
			if (ids.contains(id)) {
				ids.remove(id);
			} else {
				ids.add(id);
			}
		}

		return ids;
	}

	// Function to remove the selected taskers from the list
	@Override
	public void removeOrAccept(String uid, boolean isAccepted) {
		DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
		String uuid = FirebaseAuth.getInstance().getUid();
		if (uuid != null) {
			ref.child(uuid).child("lists").child(listName).child("assigners").child(uid).removeValue();
			ref.child(uid).child("assignedlists").child(uuid).removeValue();
		}
	}
}
