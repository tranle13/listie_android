package com.sunny.android.letran_1811project.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.custom_objects.User;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;

import java.util.ArrayList;
import java.util.Objects;

public class FriendGridAdapter extends BaseAdapter {

	// Member variables
	private final Context mContext;
	private final ArrayList<User> userList;

	// Constructor
	public FriendGridAdapter(Context context, ArrayList<User> users) {
		mContext = context;
		userList = users;
	}

	// Function to return the number of objects
	@Override
	public int getCount() {
		if (userList != null) {
			return userList.size();
		}
		return 0;
	}

	// Function to return object at certain position
	@Override
	public Object getItem(int position) {
		if (userList != null && 0 <= position || position < Objects.requireNonNull(userList).size()) {
			return userList.get(position);
		}
		return null;
	}

	// Function to return object ID
	@Override
	public long getItemId(int position) {
		return 0x1111 + position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_gridcell_friend,
					parent, false);
			vh = new ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		if (userList.get(position) != null) {
			vh.txt_FriendName.setText(userList.get(position).getFullName());
			byte[] image = userList.get(position).getProfilePicture();
			vh.ivw_FriendImage.setImageBitmap(MethodHelpers.getRoundedCornerBitmap(BitmapFactory
					.decodeByteArray(image, 0, image.length)));
		}

		return convertView;
	}

	// Recycle view pattern
	static class ViewHolder {
		final ImageView ivw_FriendImage;
		final TextView txt_FriendName;

		ViewHolder(View layout) {
			ivw_FriendImage = layout.findViewById(R.id.ivw_FriendImage);
			txt_FriendName = layout.findViewById(R.id.txt_FriendName);
		}
	}
}
