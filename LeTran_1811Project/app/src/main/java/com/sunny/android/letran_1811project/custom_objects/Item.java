package com.sunny.android.letran_1811project.custom_objects;

public class Item {

	// Member variables
	private final String name;
	private final String description;
	private final boolean isChecked;
	private String id;

	public Item() {
		name = "Name";
		description = "Description";
		isChecked = false;
		id = "i0TWHkssekhcnhwjeu-12";
	}

	// Constructor
	public Item(String _name, String _description, String _id) {
		name = _name;
		description = _description;
		isChecked = false;
		id = _id;
	}

	// Getters
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
