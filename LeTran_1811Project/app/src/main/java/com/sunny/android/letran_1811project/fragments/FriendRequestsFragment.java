package com.sunny.android.letran_1811project.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.adapters.FriendRequestAdapter;
import com.sunny.android.letran_1811project.custom_objects.User;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;

import java.util.ArrayList;
import java.util.Objects;

public class FriendRequestsFragment extends ListFragment {

	// Member variables
	private ArrayList<User> requestPeople;
	private FirebaseAuth mAuth;
	private static final String TAG = "FriendRequestsFragment";
	private static boolean isTask;
	private static String listName;

	// Constructor
	public static FriendRequestsFragment newInstance(String _listName, boolean _isTask) {
		isTask = _isTask;
		listName = _listName;
		return new FriendRequestsFragment();
	}

	// Initialize FirebaseAuth
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		mAuth = FirebaseAuth.getInstance();
	}

	// Inflate fragment's layout
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_friend_request, container, false);
	}

	// Code to get information of friend requests' profiles
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (getView() != null && mAuth.getUid() != null && getActivity() != null && getActivity().getResources() != null) {

			DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(mAuth.getUid());
			DatabaseReference _ref;
			if (isTask) {
				_ref = ref.child("lists").child(listName).child("assigners");
			} else {
				_ref = ref.child("requests");
			}

			_ref.addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					getData(dataSnapshot);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});
		}
	}

	// Hide options menu from previous fragment
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		if (isTask) {
			menu.findItem(R.id.menu_btn_delete).setVisible(false);
			menu.findItem(R.id.menu_btn_assign).setVisible(false);
			menu.findItem(R.id.menu_btn_edit).setVisible(false);
			menu.findItem(R.id.menu_btn_taskers).setVisible(false);
		} else {
			menu.findItem(R.id.menu_btn_request).setVisible(false);
		}
	}

	private void getData(DataSnapshot dataSnapshot) {
		ArrayList<String> requestUids = new ArrayList<>();
		requestPeople = new ArrayList<>();

		if (dataSnapshot.getChildrenCount() > 0) {
			for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
				requestUids.add(snapshot.getKey());
			}

			for (final String key : requestUids) {
				FirebaseDatabase.getInstance().getReference().child(key).addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
						final StorageReference stoRef = FirebaseStorage.getInstance().getReference().child(key);
						stoRef.getBytes(Long.MAX_VALUE).addOnCompleteListener(new OnCompleteListener<byte[]>() {
							@Override
							public void onComplete(@NonNull Task<byte[]> task) {
								requestPeople.add(new User(Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString(),
										Objects.requireNonNull(dataSnapshot.child("username").getValue()).toString(),
										dataSnapshot.getKey(), MethodHelpers.getImage(task, Objects.requireNonNull(getActivity()).getResources())));
								setListAdapter(new FriendRequestAdapter(getActivity(), requestPeople, isTask));
							}
						});
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: ", databaseError.toException());
					}
				});
			}
		} else {
			setListAdapter(new FriendRequestAdapter(getActivity(), requestPeople, isTask));
		}
	}
}
