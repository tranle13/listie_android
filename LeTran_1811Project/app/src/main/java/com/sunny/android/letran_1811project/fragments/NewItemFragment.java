package com.sunny.android.letran_1811project.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.custom_objects.Item;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

public class NewItemFragment extends Fragment {

	// Member variables
	private FirebaseAuth mAuth;
	private NotifyActivity mListener;
	private static String listName;

	// Constructor
	public static NewItemFragment newInstance(String _listName) {
		listName = _listName;
		return new NewItemFragment();
	}

	// Initialize FirebaseAuth and set options menu
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		mAuth = FirebaseAuth.getInstance();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof NotifyActivity) {
			mListener = (NotifyActivity)context;
		}
	}

	// Function to inflate fragment's layout
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_new_item, container, false);
	}

	// Function to hide options menu from previous fragment
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.menu_btn_delete).setVisible(false);
		menu.findItem(R.id.menu_btn_assign).setVisible(false);
		menu.findItem(R.id.menu_btn_edit).setVisible(false);
		menu.findItem(R.id.menu_btn_taskers).setVisible(false);
	}

	// Function to set up options menu and assign respective action
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_save, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_save && getContext() != null && listName != null) {
			if (MethodHelpers.isConnected(getContext()) && mAuth.getUid() != null && getView() != null) {
				EditText name = getView().findViewById(R.id.etx_ItemName);
				EditText note = getView().findViewById(R.id.etx_ItemNote);

				if (!name.getText().toString().isEmpty()) {
					DatabaseReference ref = FirebaseDatabase.getInstance().getReference(mAuth.getUid())
							.child("lists").child(listName).child("items");

					String id = ref.push().getKey();
					if (id != null) {
						ref.child(id).setValue(new Item(name.getText().toString(), note.getText().toString(), id));
					}

					if (getActivity() != null && getActivity().getCurrentFocus() != null) {
						InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
						mgr.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
					}
					mListener.newScreen(null, null,null);
				} else {
					Toast.makeText(getContext(), "Please fill in item name", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
			}
		}
		return super.onOptionsItemSelected(item);
	}
}
