package com.sunny.android.letran_1811project.fragments;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.custom_objects.User;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;

public class OtherProfileFragment extends Fragment {

	// Member variables
	private static final String TAG = "OtherProfileFragment";
	private static User chosen;
	private static boolean isFriended;
	private FirebaseAuth mAuth;

	// Constructor
	public static OtherProfileFragment newInstance(User _chosen, boolean _isFriended) {
		chosen = _chosen;
		isFriended = _isFriended;
		return new OtherProfileFragment();
	}

	// Initialize FirebaseAuth and setup options menu
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
		setHasOptionsMenu(true);
	}

	// Inflate fragment layout file
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_other_profile, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null && mAuth.getUid() != null) {
			final TextView name = getView().findViewById(R.id.txt_ProfileName);
			final TextView username = getView().findViewById(R.id.txt_ProfileUsername);
			final Button addFriend = getView().findViewById(R.id.btn_addFriend);
			((ImageView)getView().findViewById(R.id.ivw_ProfileImage)).setImageBitmap(MethodHelpers.getRoundedCornerBitmap(BitmapFactory
					.decodeByteArray(chosen.getProfilePicture(), 0, chosen.getProfilePicture().length)));

			name.setText(chosen.getFullName());
			username.setText(chosen.getUsername());

			if (isFriended) {
				addFriend.setText(R.string.friend);
				addFriend.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_friended_black_24dp, 0, 0,0);
			}

			// Code to carry action based on text on Add Friend button
			addFriend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mAuth.getUid() != null) {
						final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
						if (addFriend.getText().toString().toLowerCase().equals("friend")) {
							ref.child(mAuth.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
								@Override
								public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
									for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
										if (snapshot.getKey() != null && snapshot.getKey().equals(chosen.getUid())) {
											ref.child(mAuth.getUid()).child("friends").child(snapshot.getKey()).removeValue();
											ref.child(chosen.getUid()).child("friends").child(mAuth.getUid()).removeValue();
											Toast.makeText(getContext(), "Unfriended!", Toast.LENGTH_SHORT).show();
											addFriend.setText(R.string.btn_add_friend);
											addFriend.setCompoundDrawablesWithIntrinsicBounds
													(R.drawable.ic_person_add_black_24dp, 0, 0,0);
										}
									}
								}

								@Override
								public void onCancelled(@NonNull DatabaseError databaseError) {
									Log.w(TAG, "onCancelled: ", databaseError.toException());
								}
							});
						} else {
							ref.child(chosen.getUid()).child("requests").child(mAuth.getUid()).setValue("");
							Toast.makeText(getContext(), "Request sent!", Toast.LENGTH_SHORT).show();
						}
					}
				}
			});
		}
	}

	// Hide options menu from previous fragment
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.menu_btn_request).setVisible(false);
	}
}
