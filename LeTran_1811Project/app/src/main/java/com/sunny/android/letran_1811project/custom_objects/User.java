package com.sunny.android.letran_1811project.custom_objects;

public class User {

	// Member variables
	private final String fullname;
	private final String username;
	private final String uid;
	private final byte[] profilePicture;

	public User(String _fullname, String _username, String _uid, byte[] profilePic) {
		fullname = _fullname;
		username = _username;
		uid = _uid;
		profilePicture = profilePic;
	}

	// Getters
	public String getUsername() {
		return username;
	}

	public String getFullName() {
		return fullname;
	}

	public String getUid() {
		return uid;
	}

	public byte[] getProfilePicture() {
		return profilePicture;
	}
}
