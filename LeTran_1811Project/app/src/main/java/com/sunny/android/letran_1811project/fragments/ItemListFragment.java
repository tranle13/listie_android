package com.sunny.android.letran_1811project.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.adapters.ItemListAdapter;
import com.sunny.android.letran_1811project.custom_objects.Item;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;
import com.sunny.android.letran_1811project.interfaces.DeleteItems;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

import java.util.ArrayList;

public class ItemListFragment extends ListFragment {

	// Member variables
	private FirebaseAuth mAuth;
	private static String listName;
	private static final String TAG = "ItemListFragment";
	private NotifyActivity mListener;
	private NotifyActivity.Changes mListenerChange;
	private DeleteItems mListenerDelete;
	private ArrayList<Item> itemCollection;
	private Menu optionsMenu;
	private boolean isUtilityMenu = true;
	private static boolean isTask;
	private static String uid;

	// Constructor
	public static ItemListFragment newInstance(String _listName, boolean _isTask, String _uid) {
		listName = _listName;
		isTask = _isTask;
		uid = _uid;
		return new ItemListFragment();
	}

	// Initialize FirebaseAuth
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
		if (isTask) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
	}

	// Check if the host context implements all interfaces
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof NotifyActivity && context instanceof NotifyActivity.Changes
				&& context instanceof DeleteItems) {
			mListener = (NotifyActivity)context;
			mListenerChange = (NotifyActivity.Changes)context;
			mListenerDelete = (DeleteItems)context;
		}
	}

	// Inflate view layout for fragment
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_item_list, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null && listName != null && mAuth.getUid() != null) {

			final String otherUid;

			// Get button Complete and hide it if the list is not task and assign action if it is task
			final Button btn_complete = getView().findViewById(R.id.btn_Complete);
			if (!isTask) {
				btn_complete.setVisibility(View.GONE);
				otherUid = mAuth.getUid();
			} else {
				otherUid = uid;
				btn_complete.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mListener != null) {
							DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
							ref.child(mAuth.getUid()).child("assignedlists").child(uid).removeValue();
							ref.child(uid).child("lists").child(listName).child("assigners")
									.child(mAuth.getUid()).removeValue();
							Toast.makeText(getContext(), "Completed list!", Toast.LENGTH_SHORT).show();
							mListener.newScreen(null, null, null);
						}
					}
				});
			}

			// Get list data from Firebase and display it
			if (MethodHelpers.isConnected(getContext()) && otherUid != null) {
				DatabaseReference ref = FirebaseDatabase.getInstance().getReference(otherUid)
						.child("lists").child(listName).child("items");
				itemCollection = new ArrayList<>();

				ref.addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						itemCollection.clear();
						int isCompleted = 0;
						for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
							Item item = snapshot.getValue(Item.class);
							if (item != null) {
								item.setId(snapshot.getKey());
								itemCollection.add(item);
								if (!item.isChecked()) {
									isCompleted++;
								}
							}
						}

						// All items are completed, enable Complete button to remove task else disable it
						if (isCompleted <= 0 && itemCollection.size() > 0) {
							btn_complete.setEnabled(true);
						} else {
							btn_complete.setEnabled(false);
						}

						ItemListAdapter adapter = new ItemListAdapter(itemCollection, getContext(), 0, otherUid);
						setListAdapter(adapter);
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: " + databaseError.toString());
					}
				});
			} else {
				Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
			}
		}
	}

	// Code to display AlertDialog with item info and the ability to delete item from database
	@Override
	public void onListItemClick(ListView l, View v, final int position, long id) {
		super.onListItemClick(l, v, position, id);
		if (getActivity() != null && mAuth.getUid() != null) {
			AlertDialog.Builder building = new AlertDialog.Builder(getActivity());
			building.setTitle(itemCollection.get(position).getName());

			if (itemCollection.get(position).getDescription().isEmpty()) {
				building.setMessage("No note!");
			} else {
				building.setMessage(itemCollection.get(position).getDescription());
			}

			building.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
							.child(mAuth.getUid()).child("lists").child(listName).child("items")
							.child(itemCollection.get(position).getId());
					ref.removeValue();
					Toast.makeText(getContext(), "Deleted!", Toast.LENGTH_SHORT).show();
				}
			});

			building.setNegativeButton("Cancel", null);
			building.create().show();
		}
	}

	// Function to hide options optionsMenu from previous fragment
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		if (!isUtilityMenu) {
			menu.findItem(R.id.menu_btn_delete).setVisible(false);
			menu.findItem(R.id.menu_btn_assign).setVisible(false);
			menu.findItem(R.id.menu_btn_edit).setVisible(false);
			menu.findItem(R.id.menu_btn_taskers).setVisible(false);
		}
	}

	// Inflate the appropriate menu based on the situation
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		if (isUtilityMenu) {
			inflater.inflate(R.menu.menu_utility, menu);
		} else {
			inflater.inflate(R.menu.menu_delete, menu);
		}
		optionsMenu = menu;
	}

	//Assign respective action to each of the options items
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (getActivity() != null) {

			// If it's Delete list button then display AlertDialog to ask user if they are sure about deletion
			if (item.getItemId() == R.id.menu_btn_delete && mAuth.getUid() != null) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Delete List");
				builder.setMessage("Are you sure that you want to delete this list?");
				builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
								.child(mAuth.getUid()).child("lists");
						ref.child(listName).removeValue();
						if (mListener != null) {
							mListener.newScreen(null, null,null);
						}
					}
				});
				builder.setNegativeButton("Cancel", null);
				builder.create().show();
			}

			// If it's Edit button, change layout to edit mode and hide floating action button
			else if (item.getItemId() == R.id.menu_btn_edit) {
				if (optionsMenu != null && mListenerChange != null && itemCollection.size() > 0) {
					isUtilityMenu = false;
					onPrepareOptionsMenu(optionsMenu);
					onCreateOptionsMenu(optionsMenu, getActivity().getMenuInflater());
					ItemListAdapter adapter = new ItemListAdapter(itemCollection, getContext(), 1, "");
					setListAdapter(adapter);
					mListenerChange.hideTheFab();
				}
				else {
					Toast.makeText(getContext(), "There is nothing to edit here", Toast.LENGTH_SHORT).show();
				}
			}

			// If it's Assign button, display friend list to allow user to choose who to assign list
			else if (item.getItemId() == R.id.menu_btn_assign){
				if (mListenerChange != null) {
					mListenerChange.hideTheFab();
					getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
							.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
							.add(R.id.itemListFragmentContainer, FriendsFragment.newInstance(true, listName)).commit();
				}
			}

			// If it's Taskers button, display all friends who are assigned to list and the ability
			// to remove them
			else if (item.getItemId() == R.id.menu_btn_taskers) {
				if (mListenerChange != null) {
					mListenerChange.hideTheFab();
					getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
							.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
							.add(R.id.itemListFragmentContainer, FriendRequestsFragment.newInstance(listName,true)).commit();
				}
			}

			// If it's edit mode and user chooses to delete selected item, display AlertDialog to ask
			// if they are sure about deletion
			else if (item.getItemId() == R.id.menu_btn_delete_item){
				if (mListenerDelete != null && mListenerDelete.deleteItem(null,false).size() > 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Delete Items");
					builder.setMessage("Are you sure that you want to delete these items?");
					builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
									.child(mAuth.getUid()).child("lists").child(listName).child("items");
							for (String id : mListenerDelete.deleteItem(null,false)) {
								ref.child(id).removeValue();
							}
							if (mListener != null) {
								isUtilityMenu = true;
								mListener.newScreen(null, null,null);
								Toast.makeText(getContext(), "Deleted!", Toast.LENGTH_SHORT).show();
							}
						}
					});
					builder.setNegativeButton("Cancel", null);
					builder.create().show();
				} else {
					Toast.makeText(getContext(), "Select one or more items to delete", Toast.LENGTH_SHORT).show();
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}
}
