package com.sunny.android.letran_1811project.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.fragments.MainFragment;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;

public class ActivityActivity extends AppCompatActivity implements NotifyActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Code to check if user already logs in
		try {
			FileInputStream fis = openFileInput("listie.txt");
			String term = IOUtils.toString(fis, "UTF-8");
			fis.close();
			if (term.equals("Signed in")) {
				startNewAct();
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Code to replace container with appropriate fragment
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,
					MainFragment.newInstance()).commit();
		}
	}

	// Implement interface to get ListActivity and dismiss the login/signup activity so the
	// user couldn't go back
	@Override
	public void newScreen(String string, String _string, Boolean isTask) {
		startNewAct();
	}

	// Function to start new activity
	private void startNewAct() {
		Intent nextIntent = new Intent(this, ListsActivity.class);
		startActivity(nextIntent);
		finish();
	}
}
