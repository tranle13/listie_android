package com.sunny.android.letran_1811project.interfaces;

// Interface to communicate with ListsActivity
public interface RemoveOrAccept {
	void removeOrAccept(String uid, boolean isAccepted);
}
