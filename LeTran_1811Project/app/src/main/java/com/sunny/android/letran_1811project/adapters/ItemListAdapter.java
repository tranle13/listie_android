package com.sunny.android.letran_1811project.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.custom_objects.Item;
import com.sunny.android.letran_1811project.interfaces.DeleteItems;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

import java.util.ArrayList;
import java.util.Objects;

public class ItemListAdapter extends BaseAdapter {

	// Member variables
	private final ArrayList<Item> itemNames;
	private final Context mContext;
	private NotifyActivity.Changes mListener;
	private DeleteItems deleteItems;
	private final int identifier;
	private final String uid;

	// Constructor
	public ItemListAdapter(ArrayList<Item> _itemNames, Context _context, int _identifier, String _uid) {
		itemNames = _itemNames;
		mContext = _context;
		identifier = _identifier;
		uid = _uid;

		if (mContext instanceof NotifyActivity.Changes) {
			mListener = (NotifyActivity.Changes)_context;
		}

		if (mContext instanceof DeleteItems) {
			deleteItems = (DeleteItems)_context;
		}
	}

	// Function to return the amount of items
	@Override
	public int getCount() {
		if (itemNames != null) {
			return itemNames.size();
		}
		return 0;
	}

	// Function to return the item at specific position
	@Override
	public Object getItem(int position) {
		if (itemNames != null && 0 <= position || position < Objects.requireNonNull(itemNames).size()) {
			return itemNames.get(position);
		}
		return null;
	}

	// Function to return item ID
	@Override
	public long getItemId(int position) {
		return 0x0101 + position;
	}

	// Function to inflate the view of row
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			if (identifier == 0) {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_listrow, parent,
						false);
			} else {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_edit_items, parent,
						false);
			}
			vh = new ViewHolder(convertView, identifier);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder)convertView.getTag();
		}

		if (itemNames.get(position) != null) {
			if (identifier == 0) {
				vh.txt_ItemName.setText(itemNames.get(position).getName());
				vh.cbx_ItemComplete.setChecked(itemNames.get(position).isChecked());
				vh.cbx_ItemComplete.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mListener != null) {
							mListener.changeChecked(!itemNames.get(position).isChecked(),
									itemNames.get(position).getId(), uid);
						}
					}
				});
			} else {
				vh.cbx_ItemComplete.setText(itemNames.get(position).getName());
				vh.cbx_ItemComplete.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (deleteItems != null) {
							deleteItems.deleteItem(itemNames.get(position).getId(), false);
						}
					}
				});
			}
		}

		return convertView;
	}

	// Recycle view pattern
	static class ViewHolder {
		final CheckBox cbx_ItemComplete;
		TextView txt_ItemName;
		ViewHolder(View layout, int identifier) {
			cbx_ItemComplete = layout.findViewById(R.id.cbx_Item);
			if (identifier == 0) {
				txt_ItemName = layout.findViewById(R.id.txt_ItemName);
			}
		}
	}
}
