package com.sunny.android.letran_1811project.interfaces;

import java.util.ArrayList;

// Interface to delete selected items from list
public interface DeleteItems {
	ArrayList<String> deleteItem(String id, boolean isNow);
}
