package com.sunny.android.letran_1811project.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter {

	// Member variables
	private final Integer[] colors = {R.color.blue, R.color.bright_orange, R.color.bright_yellow,
			R.color.colorPrimaryDark, R.color.green, R.color.green_turquoise, R.color.light_orange,
			R.color.light_pink, R.color.light_yellow, R.color.lucid_red, R.color.neon_blue,
			R.color.pastel_blue, R.color.pastel_pink, R.color.pink, R.color.pure_yellow, R.color.red};
	private final ArrayList<String> lists;
	private final ArrayList<Integer> indices;
	private final Context mContext;
	private static final int BASE_ID = 0x0011;
	private NotifyActivity mListener;
	private final boolean isTask;
	private final ArrayList<String> uids;

	// Constructor
	public ListAdapter(ArrayList<String> _lists, Context _context, ArrayList<Integer> _indices,
					   boolean _isTask, ArrayList<String> _uids) {
		lists = _lists;
		indices = _indices;
		mContext = _context;
		isTask = _isTask;
		uids = _uids;

		if (_context instanceof NotifyActivity) {
			mListener = (NotifyActivity)_context;
		}
	}

	// Inflate view layout file
	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View mView = LayoutInflater.from(mContext).inflate(R.layout.custom_gridcell, viewGroup, false);
		return new ViewHolder(mView);
	}

	// Populate the UI components with data
	@Override
	public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int i) {
		if (lists.get(i) != null) {
			if (viewHolder instanceof ViewHolder) {
				ViewHolder newView = (ViewHolder)viewHolder;

				int index = 0;
				if (indices.size() > i && colors[indices.get(i)] != null) {
					index = colors[indices.get(i)];
				}
				newView.ivw_CardImage.setBackgroundResource(index);
				newView.txt_ListName.setText(lists.get(i));
				newView.cvw_Card.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mListener != null) {
							if (isTask) {
								mListener.newScreen(lists.get(viewHolder.getAdapterPosition()),
										uids.get(viewHolder.getAdapterPosition()), true);
							} else {
								mListener.newScreen(lists.get(viewHolder.getAdapterPosition()),
										"", false);
							}
						}
					}
				});
			}
		}
	}

	// Function to return item ID
	@Override
	public long getItemId(int position) {
		return BASE_ID + position;
	}

	// Function to return the number of items
	@Override
	public int getItemCount() {
		if (lists != null) {
			return lists.size();
		}
		return 0;
	}

	// Recycle view pattern
	static class ViewHolder extends RecyclerView.ViewHolder {
		final ImageView ivw_CardImage;
		final TextView txt_ListName;
		final CardView cvw_Card;

		ViewHolder(View layout) {
			super(layout);
			ivw_CardImage = layout.findViewById(R.id.ivw_ListImage);
			txt_ListName = layout.findViewById(R.id.txt_ListName);
			cvw_Card = layout.findViewById(R.id.cvw_Card);
		}
	}
}
