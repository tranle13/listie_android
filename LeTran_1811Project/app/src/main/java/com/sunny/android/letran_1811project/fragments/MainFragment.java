package com.sunny.android.letran_1811project.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sunny.android.letran_1811project.R;

public class MainFragment extends Fragment implements View.OnClickListener {

	// Constructor
	public static MainFragment newInstance() {
		return new MainFragment();
	}

	// Inflate view layout of fragment
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main, container, false);
	}

	// Set up respective action for each button
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null) {
			getView().findViewById(R.id.btn_signup).setOnClickListener(this);
			getView().findViewById(R.id.btn_login).setOnClickListener(this);
		}
	}

	// Function to replace the right fragment with the right button
	@Override
	public void onClick(View v) {
		if (getActivity() != null) {
			if (v.getId() == R.id.btn_signup) {
				getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
						.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
						.replace(R.id.fragmentContainer, SignUpFragment.newInstance()).commit();
			} else {
				getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
						.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
						.replace(R.id.fragmentContainer, LogInFragment.newInstance()).commit();
			}
		}
	}
}
