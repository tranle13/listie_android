package com.sunny.android.letran_1811project.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sunny.android.letran_1811project.R;
import com.sunny.android.letran_1811project.adapters.FriendGridAdapter;
import com.sunny.android.letran_1811project.custom_objects.User;
import com.sunny.android.letran_1811project.helpers.MethodHelpers;
import com.sunny.android.letran_1811project.interfaces.NotifyActivity;

import java.util.ArrayList;
import java.util.Objects;

public class FriendsFragment extends Fragment {

	// Member variables
	private FirebaseAuth mAuth;
	private static final String TAG = "FriendsFragment";
	private ArrayList<User> users;
	private GridView grids;
	private ConstraintLayout emptyState;
	private ArrayList<User> _users;
	private static boolean isAssigned;
	private ArrayList<String> assignedFriend;
	private NotifyActivity.Changes mListenerChange;
	private NotifyActivity mListener;
	private static String listName;

	// Constructor
	public static FriendsFragment newInstance(boolean _isAssigned, String _listName) {
		isAssigned = _isAssigned;
		listName = _listName;
		return new FriendsFragment();
	}

	// Set up options menu and FirebaseAuth
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
		setHasOptionsMenu(true);
	}

	// Check if the host context implements interface
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof NotifyActivity.Changes && context instanceof NotifyActivity) {
			mListenerChange = (NotifyActivity.Changes)context;
			mListener = (NotifyActivity)context;
		}
	}

	// Inflate fragment layout
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_friends, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && mAuth.getUid() != null && getActivity() != null) {

			// Find UI components from fragment's view
			grids = getView().findViewById(R.id.gvw_FriendList);
			emptyState = getView().findViewById(R.id.emptyState);
			final SearchView search = getView().findViewById(R.id.searchView);

			// Check if fragment is used for friend assignment and assign respective action
			if (isAssigned) {
				search.setVisibility(View.GONE);
				assignedFriend = new ArrayList<>();
			} else {
				search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
					@Override
					public boolean onQueryTextSubmit(final String query) {
						DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
						ref.addListenerForSingleValueEvent(new ValueEventListener() {
							@Override
							public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
								_users = new ArrayList<>();
								for (final DataSnapshot snapshot : dataSnapshot.getChildren()) {

									if (Objects.requireNonNull(snapshot.child("username").getValue()).toString().contains(query)) {
										final StorageReference stoRef = FirebaseStorage.getInstance().getReference().child(Objects.requireNonNull(snapshot.getKey()));
										stoRef.getBytes(Long.MAX_VALUE).addOnCompleteListener(new OnCompleteListener<byte[]>() {
											@Override
											public void onComplete(@NonNull Task<byte[]> task) {
												_users.add(new User(Objects.requireNonNull(snapshot.child("fullname").getValue()).toString(),
														Objects.requireNonNull(snapshot.child("username").getValue()).toString(),
														snapshot.getKey(), MethodHelpers.getImage(task, getResources())));

												if (_users.size() <= 0) {
													emptyState.setVisibility(View.VISIBLE);
													grids.setVisibility(View.INVISIBLE);
												} else {
													emptyState.setVisibility(View.INVISIBLE);
													grids.setVisibility(View.VISIBLE);
													grids.setAdapter(new FriendGridAdapter(getContext(), _users));
												}
											}
										});
									}
								}
							}

							@Override
							public void onCancelled(@NonNull DatabaseError databaseError) {
								Log.w(TAG, "onCancelled: ", databaseError.toException());
							}
						});
						return false;
					}

					@Override
					public boolean onQueryTextChange(String newText) {
						if (newText.isEmpty()) {
							_users = new ArrayList<>();
							grids.setAdapter(new FriendGridAdapter(getContext(), users));
							emptyState.setVisibility(View.VISIBLE);
							grids.setVisibility(View.INVISIBLE);
						}
						return false;
					}
				});
			}

			DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(mAuth.getUid())
					.child("friends");
			ref.addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					checkEmptyState(dataSnapshot);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});

			grids.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					if (isAssigned) {
						if (assignedFriend.contains(users.get(position).getUid())) {
							view.setBackgroundColor(getResources().getColor(R.color.white, null));
							assignedFriend.remove(users.get(position).getUid());
						} else {
							view.setBackgroundColor(getResources().getColor(R.color.colorPrimary, null));
							assignedFriend.add(users.get(position).getUid());
						}
					} else {
						if (search.getQuery().length() > 0) {
							getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
									.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
									.add(R.id.fragmentHolder, OtherProfileFragment.newInstance(_users.get(position), false)).commit();
						} else {
							getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
									.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
									.add(R.id.fragmentHolder, OtherProfileFragment.newInstance(users.get(position), true)).commit();
						}
					}
				}
			});
		}
	}

	// Function to check if the list of lists is empty
	private void checkEmptyState(DataSnapshot dataSnapshot) {
		if (dataSnapshot.getChildrenCount() <= 0) {
			emptyState.setVisibility(View.VISIBLE);
			grids.setVisibility(View.INVISIBLE);
		} else {
			emptyState.setVisibility(View.INVISIBLE);
			grids.setVisibility(View.VISIBLE);
			DatabaseReference _ref = FirebaseDatabase.getInstance().getReference();

			users = new ArrayList<>();
			for (DataSnapshot snapshot: dataSnapshot.getChildren()) {

				_ref.child(Objects.requireNonNull(snapshot.getKey())).addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
						final StorageReference stoRef = FirebaseStorage.getInstance().getReference().child(Objects.requireNonNull(mAuth.getUid()));
						stoRef.getBytes(Long.MAX_VALUE).addOnCompleteListener(new OnCompleteListener<byte[]>() {
							@Override
							public void onComplete(@NonNull Task<byte[]> task) {
								users.add(new User(Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString(),
										Objects.requireNonNull(dataSnapshot.child("username").getValue()).toString(),
										dataSnapshot.getKey(), MethodHelpers.getImage(task, getResources())));
								grids.setAdapter(new FriendGridAdapter(getContext(), users));
							}
						});
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: ", databaseError.toException());
					}
				});
			}
		}
	}

	// Hide the previous options menu
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		if (isAssigned) {
			menu.findItem(R.id.menu_btn_delete).setVisible(false);
			menu.findItem(R.id.menu_btn_assign).setVisible(false);
			menu.findItem(R.id.menu_btn_edit).setVisible(false);
			menu.findItem(R.id.menu_btn_taskers).setVisible(false);
		}
	}

	// Assign the right menu by checking if fragment is used for friend assignment
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		if (isAssigned) {
			inflater.inflate(R.menu.menu_assign, menu);
		} else {
			inflater.inflate(R.menu.menu_friend_request, menu);
		}
	}

	// Assign respective action to different options items
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_request && getActivity() != null) {
			getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null)
					.setCustomAnimations(R.animator.slide_in_left, 0, 0, R.animator.slide_out_right)
					.add(R.id.fragmentHolder, FriendRequestsFragment.newInstance(listName, false)).commit();
		} else if (item.getItemId() == R.id.menu_btn_assign_friend) {
			if (mListenerChange != null && mListener != null && mAuth.getUid() != null) {
				DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

				for (String uid: assignedFriend) {
					ref.child(mAuth.getUid()).child("lists").child(listName).child("assigners").child(uid).setValue("");
					ref.child(uid).child("assignedlists").child(mAuth.getUid()).setValue(listName);
				}
				mListenerChange.hideTheFab();
				mListener.newScreen(null, null,null);
			}
		}
		return super.onOptionsItemSelected(item);
	}
}
