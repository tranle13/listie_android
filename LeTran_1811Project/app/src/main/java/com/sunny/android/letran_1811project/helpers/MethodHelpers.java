package com.sunny.android.letran_1811project.helpers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.android.gms.tasks.Task;
import com.sunny.android.letran_1811project.R;

import java.io.ByteArrayOutputStream;

public class MethodHelpers {

	// Function to check if any of the EditTexts are empty
	public static int checkIfTextsEmptyOrSpaced(String email, String password,
													String username, String fullname,
													String confirmPass, int signUpOrIn) {
		String finalEmail = email.replace(" ", "");
		String finalPass = password.replace(" ", "");
		String finalUsername = username.replace(" ", "");
		String finalFullName = fullname.replace(" ", "");
		String finalConfirmPass = confirmPass.replace(" ", "");

		if (finalEmail.isEmpty() || finalPass.isEmpty()) {
			return 1;
		}

		if (signUpOrIn == 1) {
			if (finalUsername.isEmpty() || finalFullName.isEmpty()
					|| finalConfirmPass.isEmpty()) {
				return 1;
			} else if (!confirmPass.equals(password)) {
				return 2;
			}
		}
		return 0;
	}

	// Function to check if the user has internet connection
	public static boolean isConnected(Context context) {
		ConnectivityManager mgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (mgr != null) {
			NetworkInfo info = mgr.getActiveNetworkInfo();

			if (info != null) {
				return info.isConnected();
			}
		}

		return false;
	}

	// Function to round corner bitmap before assigning it to ImageView
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final Paint paint = new Paint();
		Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		RectF rectF = new RectF(rect);

		paint.setAntiAlias(true);
		canvas.drawARGB(0,0,0,0);
		canvas.drawRoundRect(rectF, (float) (bitmap.getWidth()/2), (float) (bitmap.getWidth()/2), paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	// Function to get image from FirebaseStorage
	public static byte[] getImage(Task<byte[]> task, Resources resources) {
		byte[] image;
		if (task.isSuccessful()) {
			image = task.getResult();

		} else {
			Bitmap imageBmp = BitmapFactory.decodeResource(resources, R.drawable.profile_default);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			imageBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			image = baos.toByteArray();
			imageBmp.recycle();
		}

		return image;
	}
}
