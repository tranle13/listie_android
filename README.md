# LISTIE
##### Tran Le
---
###### The main feature of the application is already said through its name. Listie basically allows user to carry out the CRUD (create, read, update, delete) operations to their lists of items which can be any types of lists such as chores lists, grocery lists and so on. However, listie is not just a list app. It allows user to add and assign friends who also use the application to different lists so that the assigned users would know which list they have to complete. Besides that, user would also have the ability to alter their personal information from their profile in the application.

---
1. **Available Features**
- Log in / sign up for new account with email, username and password.
- Add new list.
- Display all available lists.

2. **Installation considerations and requirements**: drag application APK to your emulator or physical device.

3. **Hardware considerations and requirements**: no information.

4. **Login requirements for testing**
- Log in:
- Email: hello@gmail.com
- Password: password
- Sign up: you can sign up with any email and password that you want.

5. **Testing cases**
- ***Log in / sign up***: fill in all fields and click on "Log In" ("Sign Up") button.

- ***Add new list***: click on "Lists" tab then click on plus sign (+) on the upper right corner. A dialog would show up and you can fill in the EditText with the any list name that you want then click "Add" button on the dialog.

- ***View list***: click on the card in the list on "Lists" tab.

- ***Add new item***: click on floating action button, fill in item name and click "Add".

- ***View item details***: click on the list row and an AlertDialog would pop up with the information. You also the choice to click "Delete" on the popup to delete item individually.

- ***Delete list***: click on options menu on the upper right corner, choose "Delete" and choose "Delete" on AlertDialog.

- ***Edit list***: click on options menu on the upper right corner, choose "Edit", select the unwanted items then click on trash icon on the upper right corner and choose "Delete".

- ***Assign friend***: click on options menu on the upper right corner, choose "Assign", if there are any friends, select the friends that you want and click on people icon on the upper right corner.

- ***View assigned friends***: click on options menu on the upper right corner, choose "Taskers". You will see a list of assign friends and you have the option to click on "x" button to remove that friend from the list.

- ***View assigned list***: click on "Tasks" tab and click on the card to see the list's details. Once all items are checked, you can click "Complete" button at the bottom to remove it from your tasks.

- ***View friends***: click on "Friends" tab and click on one friend, you would see new screen with friend's information and a button to send request or unfriend.

- ***View friend request***: click on the button on the upper right corner. A new screen would appear with friend request information. You would have the options to either accept or remove the request.

- ***Modify personal information***: click on "Profile tab". Click on the edit icon on the upper right corner and change the username and full name to what you want and click "Save" button. You can also sign out by clicking on "Sign Out" button.

6. **List of known bugs**: no information.

7. **Special requirements and considerations for testing**: testing on API 26 would be recommended.

